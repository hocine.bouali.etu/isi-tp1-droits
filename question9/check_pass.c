#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<string.h>

int checkpass
(char *password){

    FILE *fptr;

    fptr = fopen("/home/administrateur/passwd.txt", "r");

    if (fptr == NULL){
        printf("Cannot open file \n");
        exit(0);
    }

    char line[256];
    char *pass = strcat(password, "\n");

    while (fgets(line, sizeof(line),fptr)){
        if (strcmp(line,pass) == 0){
            fclose(fptr);
            return 0;
        }
    }

    fclose(fptr);
    return 1;
}
