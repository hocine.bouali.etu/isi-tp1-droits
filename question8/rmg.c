#include "checkpass.h"

int main(int argc, char *argv[]){

  struct stat sb;
  stat(argv[1],&sb);

  struct group *gr = getgrgid(sb.st_gid);

  __uid_t uid = getuid();
  struct passwd* pw = getpwuid(uid);
  if(pw == NULL){
    perror("getpwuid error:");
  }

  int ngroups=0;
  getgrouplist(pw->pw_name,pw->pw_gid,NULL,&ngroups);
  __gid_t groups[ngroups];
  getgrouplist(pw->pw_name,pw->pw_gid,groups,&ngroups);

  int g = 1;
  struct group *gr2;

  for (int i = 0; i<ngroups;i++){
    gr2 = getgrgid(groups[i]);
    if(gr2->gr_name == gr->gr_name){
      g = 0;
    }
  }
  if(g==1){
    printf("You don't have the right to remove the file\n");
    exit(1);
  }
  int i = checkpass(uid);
  if(i==0){
    int ret = remove(argv[1]);
    if(ret == 0){
      printf("File deleted successfully\n");
    }
    else{
      printf("Error : unable to delete the file\n");
    }
    return 0;
  }
  else{
    printf("Wrong Password\n");
    return 0;
  } 
  return 0;
}
