#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

    FILE *f;
    char Buffer[128];

    printf("%d\n",getuid());
    printf("%d\n",geteuid());
    printf("%d\n",getgid());
    printf("%d\n",getegid());
    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    f = fopen(argv[1], "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    while(fgets(Buffer,128,f)){
        printf("%s",Buffer);
    }
    fclose(f);

}

