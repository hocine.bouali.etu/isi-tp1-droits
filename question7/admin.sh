sudo -u admin bash -c 'echo "test" > dir_a/test.txt' && echo "Création de fichier dans dir_a";
sudo -u admin bash -c 'echo "test" > dir_b/test.txt' && echo "Création de fichier dans dir_b";
sudo -u admin bash -c 'echo "test" > dir_c/test.txt' && echo "Création de fichier dans dir_c";

sudo -u admin bash -c 'cat dir_a/test.txt > /dev/null' && echo "Lecture de fichier dans dir_a";
sudo -u admin bash -c 'cat dir_b/test.txt > /dev/null' && echo "Lecture de fichier dans dir_b";
sudo -u admin bash -c 'cat dir_c/test.txt > /dev/null' && echo "Lecture de fichier dans dir_c";

sudo -u admin bash -c 'touch dir_a/test2.txt' && sudo -u admin bash -c 'rm dir_a/test2.txt' && echo "Suppression de fichier dans dir_a";
sudo -u admin bash -c 'touch dir_b/test2.txt' && sudo -u admin bash -c 'rm dir_b/test2.txt' && echo "Suppression de fichier dans dir_b";
sudo -u admin bash -c 'touch dir_c/test2.txt' && sudo -u admin bash -c 'rm dir_c/test2.txt' && echo "Suppression de fichier dans dir_c";
