sudo addgroup admin;
sudo addgroup groupe_a;
sudo addgroup groupe_b;

sudo adduser admin;
sudo adduser lambda_a;
sudo adduser lambda_b;

sudo usermod -a -G admin admin
sudo usermod -a -G groupe_a lambda_a
sudo usermod -a -G groupe_b lambda_b


mkdir dir_a;
mkdir dir_b;
mkdir dir_c;

sudo chown admin:groupe_a dir_a;
sudo chown admin:groupe_b dir_b;
sudo chown admin: dir_c;

sudo chmod ug+rwx,o-rwx,g+s,+t dir_a;
sudo chmod ug+rwx,o-rwx,g+s,+t dir_b;
sudo chmod a+rwx,o-w dir_c;
