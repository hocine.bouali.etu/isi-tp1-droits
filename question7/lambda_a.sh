sudo -u lambda_a bash -c 'cat dir_a/test.txt > /dev/null' && echo "Lecture de fichier dans dir_a";
sudo -u lambda_a bash -c 'cat dir_b/test.txt > /dev/null' || echo "Echec de lecture de fichier dans dir_b";
sudo -u lambda_a bash -c 'cat dir_c/test.txt > /dev/null' && echo "Lecture de fichier dans dir_c";

sudo -u lambda_a bash -c 'touch dir_a/lambda_a.txt' && echo "Création de fichier dans dir_a";
sudo -u lambda_a bash -c 'mkdir dir_a/lambda_a' && echo "Création de répertoire dans dir_a";
sudo -u lambda_a bash -c 'echo "hello" >> dir_a/test.txt' || echo "Echec de modification de fichier sans en être propriétaire dans dir_a";
sudo -u lambda_a bash -c 'mv dir_a/test.txt dir_a/lambda_a.txt 2> /dev/null' || echo "Echec de renommage de fichier sans en être propriétaire dans dir_a";
sudo -u lambda_a bash -c 'rm -f dir_a/test.txt 2> /dev/null' || echo "Echec de suppression de fichier sans en être propriétaire dans dir_a";

sudo -u lambda_a bash -c 'touch dir_c/file.txt 2> /dev/null' || echo "Echec de création de fichier dans dir_c";
sudo -u lambda_a bash -c 'echo "hello" >> dir_c/test.txt 2> /dev/null' || echo "Echec de modification de fichier dans dir_c";
sudo -u lambda_a bash -c 'mv dir_c/test.txt dir_c/lambda_a.txt 2> /dev/null' || echo "Echec de renommage de fichier dans dir_c";
sudo -u lambda_a bash -c 'rm -f dir_c/test.txt 2> /dev/null' || echo "Echec de suppression de fichier dans dir_c";
