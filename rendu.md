# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: MEGHARI Samy samy.meghari.etu@univ-lille.fr

- Nom, Prénom, email: BOUALI Hocine hocine.bouali.etu@univ-lille.fr

## Question 1
On crée notre utilisateur à l'aide des commandes :
>sudo adduser toto
>sudo adduser toto ubuntu

On vérifie que l'utilisateur est bien présent :
>cat /etc/passwd

On vérifie que l'utilisateur appartient au groupe ubuntu :
>cat /etc/group

On change d'utilisateur :
>su - toto

On crée un fichier .txt :
> touch myfile.txt

On change les droits :
>chmod 464  myfile.txt

On essaie d'écrire
>nano myfile.txt

On constate que toto ne peut modifier myfile.txt , on en déduit que toto a besoin du droit spécifique à l'utilisateur pour écrire sur un fichier

## Question 2
Pour un répertoire le caractère x signifie qu'il est accessible ainsi que son contenu

on crée le répertoire mydir et on modifie ses droits :
>mkdir mydir
>chmod 751 mydir

on essaye d'acceder a mydir
> cd mydir

l'accés est refusé car le groupe ubuntu n'a pas les droits nécessaires.


On crée le fichier mydata.txt dans le repertoire mydir
> touch mydata.txt

On change d'utilisateur et on essaye la commande
> ls -al mydir

Toutes les infos sont sous forme de points d'interrogation car on a pas accés à mydir
 
## Question 3
>Non, le processus n'arrive pas ouvrir le fichier data.txt en lecture
toto@mon-instance:/home/ubuntu$ ./suid
EUID: 1001
EGID: 1001
RUID: 1001
RGID: 1001

Cannot open file
.

>Oui, le processus arrive ouvrir le fichier data.txt en lecture car le EUID vaut 1000 qui est celui de l'utilisateur ubuntu.

ubuntu@mon-instance:~$ chmod u+s suid
toto@mon-instance:/home/ubuntu$ ./suid
EUID: 1000
EGID: 1001
RUID: 1001
RGID: 1001

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## Question 4
On ne constate aucun changement.
EUID:  1001
EGID:  1001

## Question 5
chfn : changer les information d'un utilisatur.
>rwsr-xr-x 1 root root 85064 mai 28  2020 /usr/bin/chfn

>l’administrateur root a le droit de lire et écrire et le flag set-user-id est activé

>les utilisateur qui apartiennent au groupe root ont le droit de lecture et d'éxecution mais pas d'écriture.
>les autre utilisatuers n'ont que le droit d'éxecution.

## Question 6
Les mots de passes sont chiffrés et stockés dans le fichier /etc/shadow.
C'est un fichier texte et lisible uniquement par l'utilisateur root et présente donc moins de risque de sécurité.

## Question 7
Mettre les scripts bash dans le repertoire *question7*.
Executer dans l'ordre suivant : init, admin, lambda_a, lambda_b

## Question 8
Le programme et les scripts dans le repertoire *question8*.

script.sh lance un script pour supprimer un fichier en fonction de admin

## Question 9
Le programme et les scripts dans le repertoire *question9*.

## Question 10
Non traitée


